package es.unex.spilab.alphaprocessmining.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.ArraySet;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.unex.spilab.alphaprocessmining.alphaalgorithm.WorkflowNetwork;
import es.unex.spilab.alphaprocessmining.alphaalgorithm.tools.Event;
import es.unex.spilab.alphaprocessmining.alphaalgorithm.tools.Place;
import es.unex.spilab.alphaprocessmining.model.Pattern;

public class CheckPatternAsyncTask extends AsyncTask<Pattern, Void, Boolean> {

    private final Context mContext;
    private final WorkflowNetwork model;
    private CheckPatternAsyncTask.TaskDelegate delegate;

    public interface TaskDelegate {
        public void onFinished(boolean containsPattern);
    }


    public CheckPatternAsyncTask(Context context, WorkflowNetwork model, CheckPatternAsyncTask.TaskDelegate delegate){
        this.mContext = context;
        this.model = model;
        this.delegate = delegate;
    }


    @Override
    protected Boolean doInBackground(Pattern... patterns) {

        Pattern pattern = patterns[0];



        Map<String, Set<Place>> eventsToPlaces = model.getEventNamesToPlaceTransitionsMap();
        Map<Place, Set<Event>> placesToEvents = model.getPlaceToEventTransitionsMap();

        Boolean hasPattern = true;

        /*if patter length is 1 (one event), we only check if user perform this event*/
        if (pattern.size()==1){
            String event = pattern.get(0).trim();
            if(pattern.isNegation(0)){
                Log.i("NEGATION", event);
                Log.i("NEGATION", eventsToPlaces.keySet().toString());
                if(eventsToPlaces.containsKey(event)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                if(eventsToPlaces.containsKey(event)){
                    return true;
                }
                else{
                    return false;
                }
            }
        }

        /*we check the first event until the penultimate*/
        for ( int i = 0; i< pattern.size()-1; i++){
            String event = pattern.get(i).trim();
            String eventNext = pattern.get(i+1).trim();
            String operation = pattern.getNextOperation(i);

            boolean isNegativeA = pattern.isNegation(i);
            boolean isNegativeB = pattern.isNegation(i+1);
            //A-B
            if (!isNegativeA && !isNegativeB) {
                hasPattern = checkPatternInPositivePositive(operation, eventsToPlaces, placesToEvents, event, eventNext);
            }
            else {
                //!A-B
                if (isNegativeA && !isNegativeB) {
                    //Resto-B. For all event different to A, after B
                    hasPattern = checkPatternInNegativePositive(operation, eventsToPlaces, placesToEvents, event, eventNext);
                }
                else{
                    //A-!B
                    if (!isNegativeA && isNegativeB){
                        //It must not exists A-B
                        hasPattern = checkPatternInPositiveNegative(operation, eventsToPlaces, placesToEvents, event, eventNext);

                    }
                    else{
                        //!A-!B
                        //It must not exists Others-B
                        hasPattern = checkPatternInNegativeNegative(operation, eventsToPlaces, placesToEvents, event, eventNext);
                    }
                }
            }
            if (hasPattern==false){
                Log.i("PATTERN", "No contiene el patrón en su modelo");
                break;
            }
        }

        if (hasPattern==true){
            Log.i("PATTERN", "Contiene el patrón en su modelo");
        }

        return hasPattern;
    }

    private boolean checkPatternInPositivePositive(String operation, Map<String, Set<Place>> eventsToPlaces, Map<Place, Set<Event>> placesToEvents, String event, String eventNext) {
        boolean hasPattern = true;

        if (operation == Pattern.NEXT) {
            hasPattern = checkNextEvents(eventsToPlaces, placesToEvents, event, eventNext);
        }
        if (operation == Pattern.EXIST_NEXT) {
            hasPattern = checkRecNextEvents(eventsToPlaces, placesToEvents, event, eventNext);
        }

        return hasPattern;
    }

    private boolean checkPatternInNegativePositive(String operation, Map<String, Set<Place>> eventsToPlaces, Map<Place, Set<Event>> placesToEvents, String event, String eventNext) {
        //Resto-B. Para to'do evento distinto de A, despues B
        boolean hasPattern = true;

        Set<String> keys = eventsToPlaces.keySet();

        for(String key: keys){
            if (!key.equals(event)){
                if (operation == Pattern.NEXT) {
                    hasPattern = checkNextEvents(eventsToPlaces, placesToEvents, key, eventNext);
                }
                if (operation == Pattern.EXIST_NEXT) {
                    hasPattern = checkRecNextEvents(eventsToPlaces, placesToEvents, key, eventNext);
                }
            }
            if(hasPattern==false){
                break;
            }
        }

        return hasPattern;
    }

    private boolean checkPatternInPositiveNegative(String operation, Map<String, Set<Place>> eventsToPlaces, Map<Place, Set<Event>> placesToEvents, String event, String eventNext) {
        //Must not exists A-B
        boolean hasPattern = true;

        if (operation == Pattern.NEXT) {
            hasPattern = checkNextEvents(eventsToPlaces, placesToEvents, event, eventNext);
            Log.i("EXISTE", String.valueOf(hasPattern)+" "+event+" "+eventNext);
        }
        if (operation == Pattern.EXIST_NEXT) {
            hasPattern = checkRecNextEvents(eventsToPlaces, placesToEvents, event, eventNext);
        }

        if(hasPattern==true){
            return false;
        }
        else{
            return true;
        }
    }

    private boolean checkPatternInNegativeNegative(String operation, Map<String, Set<Place>> eventsToPlaces, Map<Place, Set<Event>> placesToEvents, String event, String eventNext) {
        //Must not exists Others-B
        boolean hasPattern = true;

        Set<String> keys = eventsToPlaces.keySet();
        boolean exist=false;

        for(String key: keys){
            if (!key.equals(event)){
                if (operation == Pattern.NEXT) {
                    exist = checkNextEvents(eventsToPlaces, placesToEvents, key, eventNext);
                }
                if (operation == Pattern.EXIST_NEXT) {
                    exist = checkRecNextEvents(eventsToPlaces, placesToEvents, key, eventNext);
                }
            }
            if(exist==true){
                hasPattern=false;
                break;
            }
        }

        return hasPattern;
    }

    private boolean checkNextEvents(Map<String, Set<Place>> eventsToPlaces, Map<Place, Set<Event>> placesToEvents, String event, String eventNext){
        boolean hasPattern = true;

        Set<Place> places = eventsToPlaces.get(event);
        if (places == null){
            hasPattern = false;
        }
        else{
            if (places.size() == 0){
                hasPattern = false;
            }
            else{
                boolean isInNextEvents = false;
                for (Place place : places){
                    Set<Event> nextEvents = placesToEvents.get(place);
                    for (Event e : nextEvents){
                        if(e.getName().equals(eventNext)){
                            isInNextEvents=true;
                            break;
                        }
                    }
                    if(isInNextEvents){
                        break;
                    }
                }
                if(isInNextEvents==false){
                    hasPattern = false;
                }
            }
        }

        return hasPattern;
    }

    private boolean checkRecNextEvents(Map<String, Set<Place>> eventsToPlaces, Map<Place, Set<Event>> placesToEvents, String event, String eventNext){
        boolean hasPattern = false;
        boolean fin = false;

        /*PSEUDO FUNCTION CODE*/
        //make a list of the event we're in
        //(A) for each event on the list, get the places we can go to and concatenate them all into a set
        //we emptied the list of events
        //if the list of places == null, end with hasPattern=false. If we don't follow
        //for each place on the set, get their events.
        //for each individual, to see if it's the one we want. If it is, isInNextEvents=True. If it is not, we put it in the initial event list
        // If isInNextEvents = true --> hasPattern = true, fin
        //If isInNextEvents=false --> repeat since (A)

        List<String> eventsToCheck = new ArrayList<>();
        eventsToCheck.add(event);
        while(!fin){
            Set<Place> placesToCheck = new HashSet<>();
            for (String e : eventsToCheck) {
                Set<Place> places = eventsToPlaces.get(e);
                if (places != null) {
                    placesToCheck.addAll(places);
                }
            }
            eventsToCheck.clear();
            if(placesToCheck.isEmpty()) {
                hasPattern = false;
                fin = true;
            }
            else {
                boolean isInNextEvents = false;
                for (Place place : placesToCheck){
                    Set<Event> nextEvents = placesToEvents.get(place);
                    for (Event e : nextEvents){
                        if(e.getName().equals(eventNext)){
                            isInNextEvents=true;
                            break;
                        }
                        else{
                            if(!eventsToCheck.contains(e.getName())){
                                eventsToCheck.add(e.getName());
                            }
                        }
                    }
                    if(isInNextEvents){
                        break;
                    }
                }
                if(isInNextEvents==true){
                    hasPattern = true;
                    fin = true;
                }
            }

        }

        return hasPattern;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        this.delegate.onFinished(false);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        this.delegate.onFinished(aBoolean);

    }
}
