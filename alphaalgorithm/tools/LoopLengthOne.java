package es.unex.spilab.alphaprocessmining.alphaalgorithm.tools;

import java.io.Serializable;

public class LoopLengthOne implements Serializable {
    private boolean initial;
    private boolean ended;
    Event prevEvent;
    Event loopedEvent;
    Event nextEvent;

    public LoopLengthOne(Event prevEvent, Event loopedEvent,
                         Event nextEvent) {
        this.prevEvent = prevEvent;
        this.loopedEvent = loopedEvent;
        this.nextEvent = nextEvent;
    }

    public LoopLengthOne(Event prevEvent, Event loopedEvent,
                         Event nextEvent, boolean initial, boolean ended) {
        this.prevEvent = prevEvent;
        this.loopedEvent = loopedEvent;
        this.nextEvent = nextEvent;
        this.initial = initial;
        this.ended = ended;
    }

    public Event getPrevEvent() {
        return prevEvent;
    }

    public void setPrevEvent(Event prevEvent) {
        this.prevEvent = prevEvent;
    }

    public Event getLoopedEvent() {
        return loopedEvent;
    }

    public void setLoopedEvent(Event loopedEvent) {
        this.loopedEvent = loopedEvent;
    }

    public Event getNextEvent() {
        return nextEvent;
    }

    public void setNextEvent(Event nextEvent) {
        this.nextEvent = nextEvent;
    }

    public boolean isInitial(){
        return this.initial;
    }

    public boolean isEnded(){
        return this.ended;
    }

    @Override
    public String toString() {
        return "LoopLengthOne [prev=" + prevEvent + ", looped="
                + loopedEvent + ", next=" + nextEvent + "]";
    }

}
