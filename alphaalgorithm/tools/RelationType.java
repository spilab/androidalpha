package es.unex.spilab.alphaprocessmining.alphaalgorithm.tools;

import java.io.Serializable;

public enum RelationType implements Serializable {
    // ->
    PRECEDES('>'),
    // <-
    FOLLOWS('<'),
    // ||
    PARALLEL('|'),
    // #
    NOT_CONNECTED('#');

    RelationType(char symbol) {
        this.sym = symbol;
    }

    private final char sym;

    public char symbol() {
        return sym;
    }
}
