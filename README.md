Alpha Algorithm for Android
==============

Implementation of the Alpha Algorithm and the Query Pattern Resolver of the Process Mining Module of the Individual Process Mining Component

This code can be added in any Android application that wants to do process mining in the phone, being able to use the proposed architecture in:

*[reference not yet available]*

Please, if you use this code, reference the article.

### Alpha algorithm

To use the Alpha algorithm on the Android device, you only need the code from the `alphaalgorithm` folder and the AsyncTask from the `asynctask/ProcessMiningAsyncTask.java` file.

This Alpha algorithm is based on the previous implementation of Blagoj Atanasovski, available from the https://github.com/atanasovskib/AlphaAlgorithm repository.

### Query Pattern Resolver

To use Query Patter Resolver with the Alpha algorithm, copy all files.

### build.gradle dependencies

You must add this dependencies to your gradle if you use the complete Process Mining Module in your app:

```    
    implementation 'org.jsoup:jsoup:1.8.3'
    implementation 'org.slf4j:slf4j-api:1.7.13'
    implementation 'org.slf4j:slf4j-jdk14:1.7.13'
    implementation 'com.google.android.material:material:1.1.0'

    implementation 'com.google.code.gson:gson:2.8.2'

    //for retrofit
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.1.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.4.1'
```

