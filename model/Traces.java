package es.unex.spilab.alphaprocessmining.model;

import java.util.ArrayList;
import java.util.List;

public class Traces {

    private String username;
    private String pattern;
    private List<List<String>> traces;

    public Traces (String username, String pattern){
        this.username = username;
        this.pattern = pattern;
        this.traces = new ArrayList<>();
    }

    public void addTrace(List<String> trace){
        this.traces.add(trace);
    }

    public int numberOfTraces() {
        return this.traces.size();
    }
}
