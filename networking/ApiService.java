package es.unex.spilab.alphaprocessmining.networking;



import es.unex.spilab.alphaprocessmining.model.Traces;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {
    @POST("traces")
    Call<ResponseBody> uploadTraces(@Body Traces traces);
}
